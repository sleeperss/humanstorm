#include <utils.inl>
#include <jobs/jobs.inl>
#include <jobs/Scheduler.h>
#include <jobs/WorkerPoll.h>

#include <cstddef>
#include <memory>
#include <thread>

enum workerType : std::size_t {
   Default
};

int main(int /* argc */, char* /*argv */[]) {
   auto const scheduler_lock = job::Scheduler::init([]() {
      job::Scheduler::worker_polls_t worker_polls { };

      worker_polls.emplace(workerType::Default, std::make_unique<job::WorkerPoll>(
            std::thread::hardware_concurrency()
      ));

      return worker_polls;
   }());

   llog::info << "To be done ;)" << std::endl;
}
